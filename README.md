CarCar offers the premiere solution for premiere solution for automobile dealership management!

If you cannot access all images in this readme follow this link
https://stream-cadmium-b6a.notion.site/README-9913f478dd694a24b570571985a05fad

CarCar Application Architecture Diagram:

[Application Architecture Diagram](https://excalidraw.com/#json=eA8A8gxeCl8wsw3ciFgVo,kSoqC6ChVMSubFnmChGAjg)

# How to start CarCar

How to run CarCar on your local machine

Fork the repository using the fork button on Gitlab

Using the terminal on your machine navigate to the folder you would like to store the application in.

When there, clone the repository to your local machine by copying the HTTPS link in the upper right hand corner on gitlab and running the command:

$ git clone <<https link>>

Once complete, use your terminal to navigate to the folder you’ve just cloned by running the following command.

$ cd project-beta

Then in the terminal on your local machine run the commands:

$ docker volume create beta-data

$ docker-compose build

$ docker-compose up

Then open your browser to [localhost:3000](http://localhost:3000) and you should see the home page for CarCar

If this does not work, please delete the associated container, volume, and images in docker and carefully repeat these steps.

## Who did what?

 The team of Software Engineers that made this application consists of:

-Taylor Shockley who was responsible for the Service Microservice

-Billy Berger-Bailey who was responsible for the Sales Microservice

## Bounded Contexts

 The application consists of 3 separate microservices  which each exist as their own bounded context within the application. So, CarCar has 3 bounded contexts. We have our Sales microservice, our Service microservice, and our Inventory microservice. In our Sales microservice, one of our models, AutomobileVO, is a value object. In our Service microservice, one of our models, AutomobileVO, is also a value object. Our Sales microservice API is accessed at the URL http://localhost:8090/api/ , which runs on ports 8080:8000. Our Service microservice API is accessed at the URL http://localhost:8080/api/, which runs on ports 8090:8000. Our Inventory microservice API is accessed at the URL http://localhost:8100/api/, which runs on ports 8100:8000.

## A word on the pollers

In both our Sales microservice and Service microservice, we have a poller that automatically gets the Automobile data from our Inventory microservice, which then updates or creates instances in our AutomobileVO data for our Sales and Service microservices that reflects what is in our Automobile data in our Inventory microservice. These pollers run every 60 seconds.

## The Automobile Sales Microservice

The models in the Automobile Sales Microservice consist of the following;

A Salesperson Model

```python
Salesperson:
    first_name - Salesperson first name
    last_name - Salesperson last name
    employee_id - Salesperson id number
    sales - sales that they've made
```

A Customer model

```python
 Customer:
    first_name - Customer first name
    last_name - CUstomer last name
    address - Customer address
    phone_number - Customer phone number
    sales - Sales made to the customer
```

An Automobile Value Object Model

```python
AutomobileVO:
    vin - the VIN number of the vehicle
    sold - If the vehicle is available or sold
    sales - the transaction associated with the vehicle if available
```

A sale model

```python
Sale:
    price- salesprice
    salesperson - which salesperson made the sale
    customer - which customer they sold it to
    automobile - the automobile they sold
```

The Automobile Sales Microservice exists as a its own bounded context to keep track of the sales side of the dealerships business. All of the sales functionalities of the dealership are grouped together to make things as simple as possible for salespeople to keep track of transactions and customers. Its api endpoints are as follows:

| Action Description | HTTP Method | URL Route |
| --- | --- | --- |
| List salespeople | GET | http://localhost:8090/api/salespeople/ |
| Create a salesperson | POST | http://localhost:8090/api/salespeople/ |
| Delete a specific salesperson | DELETE | http://localhost:8090/api/salespeople/:id/ |
| List customers | GET | http://localhost:8090/api/customers/ |
| Create a customer | POST | http://localhost:8090/api/customers/ |
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/:id/ |
| List sales | GET | http://localhost:8090/api/sales/ |
| Create a sale | POST | http://localhost:8090/api/sales/ |
| Delete a sale | DELETE | http://localhost:8090/api/sales/:id |

| Create a salesperson | POST | http://localhost:8090/api/salespeople/ |
| --- | --- | --- |

POST Sample Data                                                                                Response Data

![Screenshot 2024-03-22 at 4.53.27 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/b07e49e7-dd64-487c-b31d-79b068e6fdf2/Screenshot_2024-03-22_at_4.53.27_PM.png)

| List salespeople | GET | http://localhost:8090/api/salespeople/ |
| --- | --- | --- |

                                    List Salespeople sample response data

![Screenshot 2024-03-22 at 4.58.21 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/a770ed09-34b2-4994-afc7-b2eb8f9d85d2/Screenshot_2024-03-22_at_4.58.21_PM.png)

| Create a customer | POST | http://localhost:8090/api/customers/ |
| --- | --- | --- |

POST Sample Data                                                                                Response Data

![Screenshot 2024-03-22 at 5.03.28 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/8b1bda2d-5785-4a33-b98e-28092c7ba34e/Screenshot_2024-03-22_at_5.03.28_PM.png)

| List customers | GET | http://localhost:8090/api/customers/ |
| --- | --- | --- |

          List Customers sample response date

![Screenshot 2024-03-22 at 5.02.26 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/c1f1e08c-81cc-4dd3-8f1c-740729de2e33/Screenshot_2024-03-22_at_5.02.26_PM.png)

| Create a sale | POST | http://localhost:8090/api/sales/ |
| --- | --- | --- |

POST Sample Data                                                                                Response Data

![Screenshot 2024-03-22 at 5.10.16 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/503f8b27-bb6d-45d0-9b58-7083e97341d8/Screenshot_2024-03-22_at_5.10.16_PM.png)

| List sales | GET | http://localhost:8090/api/sales/ |
| --- | --- | --- |

List sales sample response data

![Screenshot 2024-03-22 at 5.12.00 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/0957a689-772c-41ba-b8b2-b670bb5862de/Screenshot_2024-03-22_at_5.12.00_PM.png)

## The Automobile Service Microservice



A technician model

```python
Technician
    first_name - Techncian first name
    last_name - Technician last name
    employee_id - Technician Employy id
    appointments - all appointments assoicated with the technician
```

An appointment model

```python
Appointment:
    date - the date of the appt
    time - the time of the appt
    reason - the reason for the appointment
    status - the status of the appt - created, canceled or finished
    vin - the vehicle identification number of the vehicle in need of attn
    customer - the customer who made the appt
    is_vip - indicates wether the car was purchased at the dealership
		technician - the associated technician with the appt
```

The Automobile Value Object Model  (The same as in sales)

```python
AutomobileVO:
    vin - the VIN number of the vehicle
    sold - If the vehicle is available or sold
```

The Automobile Services Microservice exists as a its own bounded context to keep track of the Service side aspects of the dealership’s business. All of the service functionalities of the dealership are grouped together to make things as simple as possible for technicians to keep track of appointments, customers, and their vehicles. For example we can track VIN numbers and appoint customers higher status bases on if they purchased their vehicle from us. Its api endpoints are as follows:

| Action Description | HTTP Method | URL Route |
| --- | --- | --- |
| List technicians | GET | http://localhost:8080/api/technicians/ |
| Create a technician | POST | http://localhost:8080/api/technicians/ |
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/ |
| List appointments | GET | http://localhost:8080/api/appointments/ |
| Create an appointment | POST | http://localhost:8080/api/appointments/ |
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/ |
| Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel/ |
| Set appointment status to "finished" | PUT | http://localhost:8080/api/appointments/:id/finish/ |

| List technicians | GET | http://localhost:8080/api/technicians/ |
| --- | --- | --- |

List technicians sample response data

![Screenshot 2024-03-22 at 5.17.01 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/a7a94244-09e6-4af4-8992-773344595331/Screenshot_2024-03-22_at_5.17.01_PM.png)

| Create a technician | POST | http://localhost:8080/api/technicians/ |
| --- | --- | --- |

POST Sample Data                                                                                Response Data

![Screenshot 2024-03-22 at 5.19.03 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/d0926b56-813d-4f10-be21-e2dc76b34c60/Screenshot_2024-03-22_at_5.19.03_PM.png)

| List appointments | GET | http://localhost:8080/api/appointments/ |
| --- | --- | --- |

                           List appointments sample response data

![Screenshot 2024-03-22 at 5.22.30 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/f9060380-86c4-4c4e-835e-9ce4012cbd01/Screenshot_2024-03-22_at_5.22.30_PM.png)

| Create an appointment | POST | http://localhost:8080/api/appointments/ |
| --- | --- | --- |

POST Sample Data                                                                                Response Data

![Screenshot 2024-03-22 at 5.24.15 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/b0f0c405-93a9-457b-98dd-a097fd09796c/Screenshot_2024-03-22_at_5.24.15_PM.png)

| Set appointment status to "canceled" | PUT | http://localhost:8080/api/appointments/:id/cancel/ |
| --- | --- | --- |

Cancel an appointment Response data

![Screenshot 2024-03-22 at 5.25.27 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/a2ae9fe8-7f7d-4ae4-83da-8cf7ec4d4014/Screenshot_2024-03-22_at_5.25.27_PM.png)

| Set appointment status to "finished" | PUT | http://localhost:8080/api/appointments/:id/finish/ |
| --- | --- | --- |

Finished appointment response data

![Screenshot 2024-03-22 at 5.32.05 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/6101e531-1c45-40c1-b64d-7b0a27b72efc/Screenshot_2024-03-22_at_5.32.05_PM.png)

## The Inventory Microservice

The inventory Microservice exists as a its own bounded context to keep track of Automobiles in our inventory and their characteristics. This is designed to make things as simple as possible for salespeople to keep track of what vehicles are available for them to sell to customers as well as to keep track of which ones are sold. Its api endpoints are as follows:

The Manufacturer model

```python
Manufacturer:
    name - the name of the manufacturer
    models- the different models from that manufacturer
```

The Vehicle Model Model

```python
VehicleModel:
    name - the name of the vehicle model
    picture_url - an image of the vehicle
    manufacturer - the manufacturer of the vehicle
    model - specific instances of the model
```

The Automobile Model

```python
Automobile:
    color - The color of the Automobile
    year - the year the automobile was made
    vin - the vehicles identification number
    sold - A binary field True/False
    model - what model the vehicle is

```

Carcar offers a simple way to build a database for your dealership allowing you to create, delete and track instances of sales, salespeople, technicians, manufacturers, models, automobiles in stock, appointments, service appointments.

| Action Description | HTTP method | URL Route |
| --- | --- | --- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/ |
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/:id/ |
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/:id/ |
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/:id/ |

| Action Description | HTTP Method | URL Route |
| --- | --- | --- |
| List vehicle models | GET | http://localhost:8100/api/models/ |
| Create a vehicle model | POST | http://localhost:8100/api/models/ |
| Get a specific vehicle model | GET | http://localhost:8100/api/models/:id/ |
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/:id/ |
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/:id/ |

| Action Description | HTTP Method | URL Route |
| --- | --- | --- |
| List automobiles | GET | http://localhost:8100/api/automobiles/ |
| Create an automobile | POST | http://localhost:8100/api/automobiles/ |
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/:vin/ |
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/:vin/ |
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/:vin/ |

| Create a vehicle manfacturer | POST | http://localhost:8100/api/manufacturers/ |
| --- | --- | --- |

Manufacturer creation in the database.                              Response Data

![Screenshot 2024-03-22 at 6.05.39 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/fadca88e-7d36-4bba-b733-39634284c69c/Screenshot_2024-03-22_at_6.05.39_PM.png)

![Screenshot 2024-03-22 at 6.06.09 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/9f197320-d1cb-40fc-9d82-9aa7b79a6280/Screenshot_2024-03-22_at_6.06.09_PM.png)

| List manufacturers | GET | http://localhost:8100/api/manufacturers/ |
| --- | --- | --- |

Response Data

![Screenshot 2024-03-22 at 6.08.42 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/e6fad70d-a2c4-494a-95d5-96d36c2cd125/Screenshot_2024-03-22_at_6.08.42_PM.png)

| Create a vehicle model | POST | http://localhost:8100/api/models/ |
| --- | --- | --- |

Creating a Vehicle model

![Screenshot 2024-03-22 at 6.09.41 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/ef2809f2-e9e8-4851-b05e-2ed089d606e6/Screenshot_2024-03-22_at_6.09.41_PM.png)

| Update a specific vehicle model | PUT | http://localhost:8100/api/models/:id/ |
| --- | --- | --- |

Updating a Vehicle model

![Screenshot 2024-03-22 at 6.10.32 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/d717e567-9832-4af8-b380-a06ae2502b9e/Screenshot_2024-03-22_at_6.10.32_PM.png)

| Get a specific vehicle model | GET | http://localhost:8100/api/models/:id/ |
| --- | --- | --- |
| Create a vehicle model | POST | http://localhost:8100/api/models/ |
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/:id/ |

Getting the detail of a vehicle model, or the return value from creating or updating a vehicle model, returns the model's information **and** the manufacturer's information.

![Screenshot 2024-03-22 at 6.13.03 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/4dd7807e-699a-4d70-bfe8-36ef085c48e9/Screenshot_2024-03-22_at_6.13.03_PM.png)

| List automobiles | GET | http://localhost:8100/api/automobiles/ |
| --- | --- | --- |

Getting a list of vehicle models returns a list of the detail information with the key "models".

To get a list of manufacturers use this endpoint.

![Screenshot 2024-03-22 at 6.13.50 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/ee18407e-cca6-468f-b568-aeadf7a2fd2b/Screenshot_2024-03-22_at_6.13.50_PM.png)

| Create an automobile | POST | http://localhost:8100/api/automobiles/ |
| --- | --- | --- |

You can create an automobile with its color, year, VIN, and the id of the vehicle model.

![Screenshot 2024-03-22 at 6.16.58 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/9719bf81-fef8-42c3-ab0f-c5e82f365576/Screenshot_2024-03-22_at_6.16.58_PM.png)

| Get a specific automobile | GET | http://localhost:8100/api/automobiles/:vin/ |
| --- | --- | --- |

As noted, you query an automobile by its VIN. For example, you would use the URL

`http://localhost:8100/api/automobiles/1C3CC5FB2AN120174/`

to get the details for the car with the VIN "1C3CC5FB2AN120174". The details for an automobile include its model and manufacturer.

![Screenshot 2024-03-22 at 6.19.04 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/884b6604-7a3d-4d8a-85d7-fc1c665cf8c1/Screenshot_2024-03-22_at_6.19.04_PM.png)

| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/:vin/ |
| --- | --- | --- |

You can update the color, year, and sold status of an automobile.

![Screenshot 2024-03-22 at 6.19.30 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/544858ce-ea84-4c15-83a4-1138fa67db65/Screenshot_2024-03-22_at_6.19.30_PM.png)

| List automobiles | GET | http://localhost:8100/api/automobiles/ |
| --- | --- | --- |

Getting a list of automobiles returns a dictionary with the key "autos" set to a list of automobile information.

![Screenshot 2024-03-22 at 6.20.05 PM.png](https://prod-files-secure.s3.us-west-2.amazonaws.com/480271ee-e502-4663-bb73-bfaa1ff68c5d/e19868bd-2e73-4d3e-870b-d32b362fb126/Screenshot_2024-03-22_at_6.20.05_PM.png)
