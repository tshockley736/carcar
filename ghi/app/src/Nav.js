import { NavLink, Link } from 'react-router-dom';
import { useState } from 'react';


function Nav() {

  const [isManufacturerOpen, setIsManufacturerOpen] = useState(false)
  const [isModelsOpen, setIsModelsOpen] = useState(false)
  const [isAutosOpen, setIsAutosOpen] = useState(false)
  const [isSalespeopleOpen, setIsSalespeopleOpen] = useState(false)
  const [isCustomersOpen, setIsCustomersOpen] = useState(false)
  const [isSalesOpen, setIsSalesOpen] = useState(false)
  const [isTechniciansOpen, setIsTechniciansOpen] = useState(false)
  const [isServiceOpen, setIsServiceOpen] = useState(false)


  const toggleManufacturersDropdown = () => {
    setIsManufacturerOpen(!isManufacturerOpen)

  }

  const toggleModelsDropdown = () => {
    setIsModelsOpen(!isModelsOpen)
  }

  const toggleAutosDropdown = () => {
    setIsAutosOpen(!isAutosOpen)
  }

  const toggleSalespeopleDropdown = () => {
    setIsSalespeopleOpen(!isSalespeopleOpen)
  }

  const toggleCustomersDropdown = () => {
    setIsCustomersOpen(!isCustomersOpen)
  }

  const toggleSalesDropdown = () => {
    setIsSalesOpen(!isSalesOpen)
  }

  const toggleTechniciansDropdown = () => {
    setIsTechniciansOpen(!isTechniciansOpen)
  }

  const toggleServiceDropdown = () => {
    setIsServiceOpen(!isServiceOpen)
  }

  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item" >
              <NavLink className="nav-link" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <NavLink onClick={toggleManufacturersDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/manufacturers" role="button" data-bs-toggle="dropdown">Manufacturers</NavLink>
              <ul className={isManufacturerOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleManufacturersDropdown} className="dropdown-item" to="/manufacturers">List of Manufacturers</Link></li>
                <li><Link onClick={toggleManufacturersDropdown} className="dropdown-item" to="/manufacturers/new">Create a Manufacturer</Link></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink onClick={toggleModelsDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/models" role="button" data-bs-toggle="dropdown">Models</NavLink>
              <ul className={isModelsOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleModelsDropdown} className="dropdown-item" to="/models">List of Models</Link></li>
                <li><Link onClick={toggleModelsDropdown} className="dropdown-item" to="/models/new">Create a Model</Link></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
            <NavLink onClick={toggleAutosDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/automobiles" role="button" data-bs-toggle="dropdown">Automobiles</NavLink>
            <ul className={isAutosOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleAutosDropdown} className="dropdown-item" to="/automobiles">List of Automobiles</Link></li>
                <li><Link onClick={toggleAutosDropdown} className="dropdown-item" to="/automobiles/new">Create an Automobile</Link></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink onClick={toggleSalespeopleDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/salespeople" role="button" data-bs-toggle="dropdown">Salespeople</NavLink>
              <ul className={isSalespeopleOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleSalespeopleDropdown} className="dropdown-item" to="/salespeople">List of Salespeople</Link></li>
                <li><Link onClick={toggleSalespeopleDropdown} className="dropdown-item" to="/salespeople/new">Add a Salesperson</Link></li>
                <li><Link onClick={toggleSalespeopleDropdown} className="dropdown-item" to="/sales/history">Salesperson History</Link></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink onClick={toggleCustomersDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/customers" role="button" data-bs-toggle="dropdown">Customers</NavLink>
              <ul className={isCustomersOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleCustomersDropdown} className="dropdown-item" to="/customers">List of Customers</Link></li>
                <li><Link onClick={toggleCustomersDropdown} className="dropdown-item" to="/customers/new">Add a Customer</Link></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink onClick={toggleSalesDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/sales" role="button" data-bs-toggle="dropdown">Sales</NavLink>
              <ul className={isSalesOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleSalesDropdown} className="dropdown-item" to="/sales">List of Sales</Link></li>
                <li><Link onClick={toggleSalesDropdown} className="dropdown-item" to="/sales/new">Record a New Sale</Link></li>
                <li><Link onClick={toggleSalesDropdown} className="dropdown-item" to="/sales/history">Salesperson History</Link></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink onClick={toggleTechniciansDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/technicians" role="button" data-bs-toggle="dropdown">Technicians</NavLink>
              <ul className={isTechniciansOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleTechniciansDropdown} className="dropdown-item" to="/technicians">List of Technicians</Link></li>
                <li><Link onClick={toggleTechniciansDropdown} className="dropdown-item" to="/technicians/new">Add a Technician</Link></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink onClick={toggleServiceDropdown} className="nav-link dropdown-toggle" aria-current="page" to="/appointments" role="button" data-bs-toggle="dropdown">Service Appointments</NavLink>
              <ul className={isServiceOpen ? "dropdown-menu show" : "dropdown-menu"}>
                <li><Link onClick={toggleServiceDropdown} className="dropdown-item" to="/appointments">Upcoming Service Appointments</Link></li>
                <li><Link onClick={toggleServiceDropdown} className="dropdown-item" to="/appointments/new">Schedule a Service Appointment</Link></li>
                <li><Link onClick={toggleServiceDropdown} className="dropdown-item" to="/appointments/history">Service History</Link></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
