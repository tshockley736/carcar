import React, { useState } from 'react';


function TechnicianForm() {
    const [error, setError] = useState(false)
    const [submitted, setSubmitted] = useState(false)
    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        employee_id: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();
        const locationUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            setSubmitted(true)
            setFormData({
                first_name: '',
                last_name: '',
                employee_id: '',
            });
        } else {
            if (response.status === 406) {
                setError(true)
                setSubmitted(false)
            }
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const showMessage = submitted ? 'alert alert-success mt-4 offset-3 col-6' : 'alert alert-success d-none mb-0 offset-3 col-6';
    const showError = error ? 'alert alert-danger mb-0 mt-4 offset-3 col-6' : 'alert alert-danger d-none mb-0 offset-3 col-6';

    return (
        <>
            <div className={showMessage} id="form-success">
                Successfully Added a New Technician
            </div>
            <div className={showError} id="form-success">
                Technician employee ID already exists. Please choose another ID.
            </div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a Technician</h1>
                        <form onSubmit={handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.first_name} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" />
                                <label htmlFor="first_name">First Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange} value={formData.last_name} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" />
                                <label htmlFor="last_name">Last Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFormChange}  value={formData.employee_id} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" />
                                <label htmlFor="employee_id">Employee ID</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

export default TechnicianForm;
