import { useEffect, useState } from 'react';


function SalespersonList() {
    const [salespeople, setSalespeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/');

        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }

    useEffect(()=>{
        getData()
      }, []);

    return (
        <>
            <h1 className="mt-4 mb-4">Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                        <tr key={salesperson.id}>
                            <td>{salesperson.first_name }</td>
                            <td>{salesperson.last_name }</td>
                            <td>{salesperson.employee_id}</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SalespersonList
