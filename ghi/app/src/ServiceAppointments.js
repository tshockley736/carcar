import { useEffect, useState } from 'react';


function ServiceAppointments() {
    const [appointments, setAppointments] = useState([]);
    const [hasCancelled, setHasCancelled] = useState({
        date_time: '',
        vin: '',
    });
    const [hasFinished, setHasFinished] = useState({
        date_time: '',
        vin: '',
    });

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');

        if (response.ok) {
            const data = await response.json();

            const serviceAppointments = []
            for (let appointment of data.appointments) {
                if (appointment.status === "created") {
                    serviceAppointments.push(appointment)
                }
            }
            setAppointments(serviceAppointments)
        }
    }

    useEffect(()=>{
        getData();
    }, []);

    const handleCancel = async (e) => {
        const href = e.target.value
        const date_time = e.target.name
        const vin = e.target.id

        const cancelUrl = `http://localhost:8080${href}cancel/`;

        const response = await fetch(cancelUrl, {method:"put"})
            if (response.ok) {
                setHasCancelled({
                    date_time: date_time,
                    vin: vin})
                getData();
                setHasFinished([]);
            }
    }

    const handleFinish = async (e) => {
        const href = e.target.value
        const date_time = e.target.name
        const vin = e.target.id

        const finishUrl = `http://localhost:8080${href}finish/`;

        const response = await fetch(finishUrl, {method:"put"})
            if (response.ok) {
                setHasFinished({
                    date_time: date_time,
                    vin: vin})
                getData();
                setHasCancelled([]);
            }
    }

    const hideCancel = hasCancelled.vin ? 'alert alert-success mb-0' : 'alert alert-success d-none mb-0';
    const hideFinish = hasFinished.vin ? 'alert alert-success mb-0' : 'alert alert-success d-none mb-0';
    const hideTable = appointments.length ? "table table-striped" : "table table-striped d-none";
    const noAppt = appointments.length ? 'alert alert-warning d-none mb-0' : 'alert alert-warning mb-0'

    return (
        <>
            <h1 className="mt-4 mb-4">Service Appointments</h1>

            <div className={hideCancel} value={hasCancelled} id="cancelled-message">
                Appointment for VIN# {hasCancelled.vin} on {new Date(hasCancelled.date_time).toLocaleDateString()} at {new Date(hasCancelled.date_time).toLocaleTimeString()} Cancelled
            </div>
            <div className={hideFinish} value={hasFinished} id="finished-message">
                Appointment for VIN# {hasFinished.vin} on {new Date(hasFinished.date_time).toLocaleDateString()} at {new Date(hasFinished.date_time).toLocaleTimeString()} Finished
            </div>
            <div className={noAppt} id="finished-message">
                You have no more service appointments scheduled.
            </div>
            <table className={hideTable}>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => {
                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            {appointment.is_vip ? <td>Yes</td> : <td>No</td>}
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td><button className="btn btn-primary" value={appointment.href} name={appointment.date_time} id={appointment.vin} onClick={handleCancel}>Cancel</button></td>
                            <td><button className="btn btn-primary" value={appointment.href} name={appointment.date_time} id={appointment.vin} onClick={handleFinish}>Finish</button></td>
                        </tr>
                    );
                })}
                </tbody>
            </table>
        </>
    )
}

export default ServiceAppointments;
