from django.urls import path

from .api_views import (
    api_list_technicians,
    api_technician,
    api_list_appointments,
    api_appointment,
    api_cancel_appointment,
    api_finish_appointment,
    api_list_autoVO,
)

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_list_appointments, name="api_list_appointments"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path(
        "appointments/<int:pk>/cancel/",
        api_cancel_appointment,
        name="api_cancel_appointment"
        ),
    path(
        "appointments/<int:pk>/finish/",
        api_finish_appointment,
        name="api_finish_appointment"
        ),
    path("automobilevos/", api_list_autoVO, name="api_list_autoVO"),
]
